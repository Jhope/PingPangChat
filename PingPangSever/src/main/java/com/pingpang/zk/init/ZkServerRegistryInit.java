package com.pingpang.zk.init;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.pingpang.redis.RedisPre;
import com.pingpang.zk.ZkServerRegistry;
import com.pingpang.zk.util.IPGetUtil;

@Component
@DependsOn("springContextUtil")
public class ZkServerRegistryInit implements InitializingBean{

	private Logger logger = LoggerFactory.getLogger(ZkServerRegistryInit.class);
	
	/** zookeeper地址 */
	@Value("${zk.address}")
	private  String zkServer;
	
	@Value("${netty.ip}")
	private String neetyIP;
	
	@Value("${netty.port}")
	private String neetyPort;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		logger.info("----------注册服务端开始--------------------------------------------");
		logger.info("----------注册IP:["+IPGetUtil.getIp()+":"+neetyPort+"]");
		//logger.info("----------注册IP:["+IPGetUtil.getV4IP()+":"+neetyPort+"]");
		ZkServerRegistry zk=ZkServerRegistry.getServerRegistry(zkServer);
		zk.watchServer(RedisPre.SERVER_ADDRES_ZSET);
		//zk.removeNode(RedisPre.SERVER_ADDRES_MAP,IPGetUtil.getV4IP()+":"+neetyPort);
		zk.removeNode(RedisPre.SERVER_ADDRES_ZSET,neetyIP+":"+neetyPort);
		//zk.crateNode(RedisPre.SERVER_ADDRES_MAP,IPGetUtil.getV4IP()+":"+neetyPort);
		zk.crateNode(RedisPre.SERVER_ADDRES_ZSET,neetyIP+":"+neetyPort);
		logger.info("----------注册服务端结束--------------------------------------------");
	}
}
