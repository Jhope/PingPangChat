/**
 * 直播服务
 */
var txtSelfId = document.querySelector("input#txtSelfId");
var txtTargetId = document.querySelector("input#txtTargetId");

let peer = null;
let localConn = null;
let localStream = null;

hashCode = function (str) {
    var hash = 0;
    if (str.length == 0) return hash;
    for (i = 0; i < str.length; i++) {
        char = str.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash;
    }
    return hash;
}


let connOption = { host: '139.159.144.33', port: 9000, path: '/', debug: 3 };

function audioCall() {
	
    if (!peer) {
    	peer = new Peer(hashCode(txtSelfId.value), connOption);
    	var call = peer.connect(hashCode(txtTargetId.value));
    	
    	call.on('open', function() {
    		call.send(hashCode(txtSelfId.value));
    	});
    	
        peer.on('call', function (call) {
            call.answer();
            call.on('stream', function (stream) {
            	console.log('received remote stream');
            	remoteVideoVid.srcObject = stream;
            	//解决报错The play() request was interrupted by a new load request.
            	setTimeout(function(){
            		remoteVideoVid.play();
            	},"150");
            });    
            
        });
        
            
    }
}

setTimeout("audioCall()","1000");

